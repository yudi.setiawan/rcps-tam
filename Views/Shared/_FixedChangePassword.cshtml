﻿ @model ChangePasswordModel
@using TAMRCPS.Web.Models

@{
    var http = HttpContext.Current.Request.IsSecureConnection ? "https://" : "http://";
    var serverAddress = HttpContext.Current.Request.Url.Authority;
    var url = http + serverAddress;
}
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>.:: TAM RCPS - Change Password ::.</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="~/Content/plugins/bootstrap-3.2.0-dist/css/bootstrap.min.css" rel="stylesheet" />
    <link href="~/Content/plugins/font-awesome-4.2.0/css/font-awesome.min.css" rel="stylesheet" />
    <link href="~/Content/login.css" rel="stylesheet" />
    <link rel="icon" href="~/Content/images/company/tam_favicon.ico" type="image/x-icon">
    @Html.Partial("_html5shiv")
</head>
<body>
    <div class="container">
        @*<div class="row navbar-fixed-top frm_header clearfix">
            <div class="frm_top">
                <span><img src="~/Content/images/company/tam_logo_nav.png" height="45" alt="Toyota Part Center" /></span>
                <span class="pull-right frm_title"> <img src="~/Content/images/company/tam_logo_transparent.gif" height="45" /> <b class="lato">SUPPLIER PORTAL SYSTEM</b></span>
            </div>
        </div>*@
        <div class="row frm_login">
            <div class="col-md-6 col-md-4 col-md-offset-4 span8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong class="">Change Password</strong>
                    </div>
                    <div class="panel-body">
                        <form method="post" id="passwordForm">
                            <input style="margin-top:3px;" type="password" class="input-md form-control" name="password" id="password" placeholder="Old Password" autocomplete="off">

                            <input style="margin-top:3px;" type="password" class="input-md form-control" name="password1" id="password1" placeholder="New Password" autocomplete="off">
                            <div class="row">
                                <div class="col-md-6">
                                    <span id="8char" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> Min 8 Chars<br>
                                    <span id="15char" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> Max 15 Chars<br>
                                </div>
                                <div class="col-md-6">
                                    <span id="ucase" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> Alphabet<br>
                                    <span id="num" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> Numeric
                                </div>
                            </div>
                            <input style="margin-top:3px;" type="password" class="input-md form-control" name="password2" id="password2" placeholder="Repeat Password" autocomplete="off">
                            <div class="row">
                                <div class="col-md-12">
                                    <span id="pwmatch" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> Passwords Match
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <button id="btnSubmit" class="btn btn-primary btn-md col-md-12"><i class="fa fa-lock"></i> Change Password</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row navbar-fixed-bottom  text-center frm_footer">
            Copyright &copy; 2016 Toyota Astra Motor. All rights reserved.
        </div>
    </div>
    <script src="~/Scripts/jquery-2.1.1.min.js"></script>
    <script src="~/Content/plugins/bootstrap-3.2.0-dist/js/bootstrap.min.js"></script>

    <script type="text/javascript">
        var ucase = new RegExp("[a-zA-Z]+");
        var num = new RegExp("[0-9]+");

        function IsMinChar() {
            if ($("#password1").val().length >= 8) {
                $("#8char").removeClass("glyphicon-remove");
                $("#8char").addClass("glyphicon-ok");
                $("#8char").css("color", "#00A41E");
                return true;
            } else {
                $("#8char").removeClass("glyphicon-ok");
                $("#8char").addClass("glyphicon-remove");
                $("#8char").css("color", "#FF0004");
                return false;
            }
        }

        function IsMaxChar() {
            if ($("#password1").val().length <= 15) {
                $("#15char").removeClass("glyphicon-remove");
                $("#15char").addClass("glyphicon-ok");
                $("#15char").css("color", "#00A41E");
                return true;
            } else {
                $("#15char").removeClass("glyphicon-ok");
                $("#15char").addClass("glyphicon-remove");
                $("#15char").css("color", "#FF0004");
                return false;
            }
        }

        function IsAlphabet() {
            if (ucase.test($("#password1").val())) {
                $("#ucase").removeClass("glyphicon-remove");
                $("#ucase").addClass("glyphicon-ok");
                $("#ucase").css("color", "#00A41E");
                return true;
            } else {
                $("#ucase").removeClass("glyphicon-ok");
                $("#ucase").addClass("glyphicon-remove");
                $("#ucase").css("color", "#FF0004");
                return false;
            }
        }

        function IsNumeric() {
            if (num.test($("#password1").val())) {
                $("#num").removeClass("glyphicon-remove");
                $("#num").addClass("glyphicon-ok");
                $("#num").css("color", "#00A41E");
                return true;
            } else {
                $("#num").removeClass("glyphicon-ok");
                $("#num").addClass("glyphicon-remove");
                $("#num").css("color", "#FF0004");
                return false;
            }
        }

        function IsMatch() {
            if ($("#password1").val() == $("#password2").val()) {
                $("#pwmatch").removeClass("glyphicon-remove");
                $("#pwmatch").addClass("glyphicon-ok");
                $("#pwmatch").css("color", "#00A41E");
                return true;
            } else {
                $("#pwmatch").removeClass("glyphicon-ok");
                $("#pwmatch").addClass("glyphicon-remove");
                $("#pwmatch").css("color", "#FF0004");
                return false;
            }
        }


        $("#password1").keyup(function () {
            IsMinChar();
            IsMaxChar();
            IsAlphabet();
            IsNumeric();
        });

        $("#password2").keyup(function () {
            IsMatch();
        });

        $("#btnSubmit").click(function (e) {
            if (IsMinChar() && IsMaxChar() && IsAlphabet() && IsNumeric() && IsMatch()) {
                e.preventDefault();
                $.ajax({
                    url: '@Url.Action("AjaxChangePassword", "User")',
                    type: 'GET',
                    dataType: 'json',
                    data: { OldPassword: $("#password").val(), NewPassword: $("#password1").val() },
                    success: function (results) {
                        console.log(results);
                        //alert(results.Status);
                        if (results.Status) {
                            alert('Password was successfully changed. Please Login again.');
                            var url = "@url"
                            window.location.href = url + "/Logout";
                        } else {
                            alert('Password was failed changed.');
                        }
                    }
                });

            } 
        });
    </script>


</body>
</html>

