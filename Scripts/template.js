var ToggleState = false;
var DataTableState = false;

function scrollToHeader() {
    var scrollTop = ($('#divContent').offset().top) - 55;
    $('html,body').animate({
        scrollTop: scrollTop
    }, 1000);
}

function isNumber(x) {
    return x % 1 === 0;
}

function isValidInput(inputType, inputText) {
    var status = false;
    if (inputText.length == 0) {
        status = true;
    }
    else
    {
        switch (inputType.toLowerCase()) {
        case "ponumber":
            status = (/^[a-z0-9A-Z]{8}$/.test(inputText));
            break;
        case "asnnumber":
            status = (/^[a-z0-9A-Z]{11}$/.test(inputText));
            break;
        case "numeric":
            status = (/^[0-9]$/.test(inputText));
            break;
        case "email":
            status = (/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i.test(inputText));
            break;
        case "password":
            status = (/^[A-Za-z0-9]{8,15}$/.test(inputText));
            break;
        }
    }
    return status;
}

function getLINQ(obj) {
    var json = JSON.parse(obj);
    var count = Object.keys(json).length;
    var where = "";
    for (var i = 0; i < count; i++) {
        if (json[i].Value.length > 0 && json[i].Value != '\'\'' && json[i].Value != '\"\"') {
            where = where + json[i].name + " " + json[i].operator + " " + json[i].Value;
        } else {
            if (json[i].Value == '\"\"') {
                where = where + json[i].name + " != NULL";
            } else {
                where = where + json[i].name + " IS NOT NULL";
            }
        }

        if (i != (count - 1)) {
            where = where + " AND ";
        }
    }
    return where;
}

function removeA(arr) {
    var what;
    var a = arguments;
    var l = a.length;
    var ax;
    while (l > 1 && arr.length) {
        what = a[--l];
        while ((ax = arr.indexOf(what)) !== -1) {
            arr.splice(ax, 1);
        }
    }
    return arr;
}

function RefreshDataTable(div) {
    var table = $(div).DataTable();
    table.draw();
}

function ResetDataTable(div) {
    var table = $(div).DataTable();
    table.row('.selected').remove().draw();
}

function GenerateTFoot(j) {
    var html = "";

    for (var i = 0; i < j; i++) {
        html = html + "<th></th>";
    }

    return html;
}

function GeneratePeriod(diff) {
    var date = new Date();
    var month = date.getMonth();
    var year = date.getFullYear();

    var period = [];

    for (var i = month; i > (month - diff) ; i--) {
        period.push(year + '' + zeroPad(i, 2));
    }
    return JSON.stringify(period);
}

function zeroPad(num, places) {
    var zero = places - num.toString().length + 1;
    return Array(+(zero > 0 && zero)).join("0") + num;
}

$.fn.serializeObjectIntoJson = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

$("#btnClose").click(function () {
    ZoomOut();
    $("#divContent").hide();
});

function ZoomOut() {
    $('.left-side').removeClass("collapse-left");
    $(".right-side").removeClass("strech");
    ToggleState = false;
}

function ZoomIn() {
    $('.left-side').addClass("collapse-left");
    $(".right-side").addClass("strech");
    ToggleState = true;
}

function formValidator(div, tbl, url) {
    $(div).bootstrapValidator({
        message: 'This form is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            //Role
            Name: {
                validators: {
                    notEmpty: {
                        message: 'The Role Group is required'
                    }
                }
            },
            Active: {
                validators: {
                    notEmpty: {
                        message: 'The Active is required'
                    }
                }
            },
            SystemName: {
                validators: {
                    notEmpty: {
                        message: 'The Name is required'
                    }
                }
            },
            IsVendor: {
                validators: {
                    notEmpty: {
                        message: 'The Role Type is required'
                    }
                }
            },
            //Resource
            Code: {
                validators: {
                    notEmpty: {
                        message: 'The Code is required'
                    }
                }
            },
            Value: {
                validators: {
                    notEmpty: {
                        message: 'The Value is required'
                    }
                }
            },
            Language: {
                validators: {
                    notEmpty: {
                        message: 'The Language is required'
                    }
                }
            },
            //ModulePlant
            VendorGroup: {
                validators: {
                    notEmpty: {
                        message: 'The Vendor Group is required'
                    },
                    numeric: {
                        message: 'The VendorGroup must be a numeric'
                    }
                }
            },
            VendorCode: {
                validators: {
                    notEmpty: {
                        message: 'The Vendor Code is required'
                    },
                    numeric: {
                        message: 'The VendorCode must be a numeric'
                    }
                }
            },
            Module: {
                validators: {
                    notEmpty: {
                        message: 'The Module is required'
                    },
                    numeric: {
                        message: 'The Module must be a numeric'
                    }
                }
            },
            //Menu
            MenuName: {
                validators: {
                    notEmpty: {
                        message: 'The Menu Name is required'
                    }
                }
            },
            ControllerName: {
                validators: {
                    notEmpty: {
                        message: 'The Controller Name is required'
                    }
                }
            },
            ActionName: {
                validators: {
                    notEmpty: {
                        message: 'The Action Name is required'
                    }
                }
            },
            Title: {
                validators: {
                    notEmpty: {
                        message: 'The Title is required'
                    }
                }
            },
            //Document
            Module: {
                validators: {
                    notEmpty: {
                        message: 'The Module is required'
                    },
                    numeric: {
                        message: 'The Module must be a numeric'
                    }
                }
            },
            Vendor: {
                validators: {
                    notEmpty: {
                        message: 'The Vendor is required'
                    },
                    numeric: {
                        message: 'The Vendor must be a numeric'
                    }
                }
            },
            //ApplicationParam
            Code: {
                validators: {
                    notEmpty: {
                        message: 'The Code is required'
                    }
                }
            },
            Value: {
                validators: {
                    notEmpty: {
                        message: 'The Value address is required'
                    }
                }
            },
            ParentId: {
                validators: {
                    notEmpty: {
                        message: 'The Parent Id is required'
                    },
                    numeric: {
                        message: 'The Parent Id must be a numeric'
                    }
                }
            },
            Description: {
                validators: {
                    notEmpty: {
                        message: 'The Description is required'
                    }
                }
            },
            //ActivityLog
            ActivityLogTypeId: {
                validators: {
                    notEmpty: {
                        message: 'The Activity Log Type Id is required'
                    },
                    numeric: {
                        message: 'The Activity Log Type Id must be a numeric'
                    }
                }
            },
            UserId: {
                validators: {
                    notEmpty: {
                        message: 'The User Id is required'
                    },
                    numeric: {
                        message: 'The User Id must be a numeric'
                    }
                }
            },
            Comment: {
                validators: {
                    notEmpty: {
                        message: 'The Comment is required'
                    }
                }
            },
            //DeliveryDate
            YearMonth: {
                validators: {
                    notEmpty: {
                        message: 'The Year Month is required'
                    },
                    numeric: {
                        message: 'The Year Month must be a numeric'
                    }
                }
            },
            MonthSeq: {
                validators: {
                    notEmpty: {
                        message: 'The Month Seq is required'
                    },
                    numeric: {
                        message: 'The Month Seq must be a numeric'
                    }
                }
            },
            OrderSending: {
                validators: {
                    notEmpty: {
                        message: 'The Order Sending is required'
                    }
                }
            },
            RevisedType: {
                validators: {
                    notEmpty: {
                        message: 'The Revised Type is required'
                    }
                }
            },
            DocumentId: {
                validators: {
                    notEmpty: {
                        message: 'The Document Id is required'
                    }
                }
            },
            Version: {
                validators: {
                    notEmpty: {
                        message: 'The Version is required'
                    },
                    numeric: {
                        message: 'The Version must be a numeric'
                    }
                }
            },
            //GoodReceive
            DocumentId: {
                validators: {
                    notEmpty: {
                        message: 'The Document Id is required'
                    },
                    numeric: {
                        message: 'The Document Id must be a numeric'
                    }
                }
            },
            //Module
            ModuleName: {
                validators: {
                    notEmpty: {
                        message: 'The Module Name is required'
                    }
                }
            },
            DeptHeadApproval: {
                validators: {
                    notEmpty: {
                        message: 'The Deptartment Head Approval is required'
                    }
                }
            },
            //Notification
            Subject: {
                validators: {
                    notEmpty: {
                        message: 'The Subject is required'
                    }
                }
            },
            Message: {
                validators: {
                    notEmpty: {
                        message: 'The Message is required'
                    }
                }
            },
            Url: {
                validators: {
                    notEmpty: {
                        message: 'The Url is required'
                    }
                }
            },
            CreateTo: {
                validators: {
                    notEmpty: {
                        message: 'The Create To is required'
                    }
                }
            },
            DocumentId: {
                validators: {
                    notEmpty: {
                        message: 'The Document Id is required'
                    }
                }
            },
            IsRead: {
                validators: {
                    notEmpty: {
                        message: 'The Is Readable is required'
                    }
                }
            },
            Module: {
                validators: {
                    notEmpty: {
                        message: 'The Module is required'
                    }
                }
            },
            //PermissionRecord
            Name: {
                validators: {
                    notEmpty: {
                        message: 'The Name is required'
                    }
                }
            },
            SystemName: {
                validators: {
                    notEmpty: {
                        message: 'The System Name is required'
                    }
                }
            },
            Menu: {
                validators: {
                    notEmpty: {
                        message: 'The Menu is required'
                    },
                    numeric: {
                        message: 'The Menu must be a numeric'
                    }
                }
            },
            //OrderSending
            DocumentId: {
                validators: {
                    notEmpty: {
                        message: 'The Document Id is required'
                    }
                }
            },
            OrderDate: {
                validators: {
                    notEmpty: {
                        message: 'The Order Date is required'
                    },
                    date: {
                        message: 'The Order Date must be a datetime'
                    }
                }
            },
            VendorCode: {
                validators: {
                    notEmpty: {
                        message: 'The Vendor Code is required'
                    }
                }
            },
            PoNum: {
                validators: {
                    notEmpty: {
                        message: 'The PO Num is required'
                    }
                }
            },
            ItemNo: {
                validators: {
                    notEmpty: {
                        message: 'The Item Number is required'
                    },
                    numeric: {
                        message: 'The Item Number must be a numeric'
                    }
                }
            },
            Fran: {
                validators: {
                    notEmpty: {
                        message: 'The Fran is required'
                    }
                }
            },
            PartsNo: {
                validators: {
                    notEmpty: {
                        message: 'The Parts Number is required'
                    }
                }
            },
            PalletizeMark: {
                validators: {
                    notEmpty: {
                        message: 'The Palletize Mark is required'
                    }
                }
            },
            KanbanCardQty: {
                validators: {
                    notEmpty: {
                        message: 'The Kanban Card Quantity is required'
                    },
                    numeric: {
                        message: 'The Kanban Card Quantity must be a numeric'
                    }
                }
            },
            POQty: {
                validators: {
                    notEmpty: {
                        message: 'The PO Quantity is required'
                    },
                    numeric: {
                        message: 'The PO Quantity must be a numeric'
                    }
                }
            },
            DNNo: {
                validators: {
                    notEmpty: {
                        message: 'The DN Number is required'
                    }
                }
            },
            OrderType: {
                validators: {
                    notEmpty: {
                        message: 'The Order Type is required'
                    }
                }
            },
            OrderStatus: {
                validators: {
                    notEmpty: {
                        message: 'The Order Status is required'
                    }
                }
            },
            KanbanDeliveryDate: {
                validators: {
                    notEmpty: {
                        message: 'The Kanban Delivery Date is required'
                    },
                    DateTime: {
                        message: 'The Kanban Delivery Date must be a datetime'
                    }
                }
            },
            KanbanDeliveryQty: {
                validators: {
                    notEmpty: {
                        message: 'The Kanban Delivery Quantity is required'
                    },
                    numeric: {
                        message: 'The Kanban Delivery Quantity must be a numeric'
                    }
                }
            },
            CustomerCode: {
                validators: {
                    notEmpty: {
                        message: 'The Customer Code is required'
                    }
                }
            },
            CustomerOrderNo: {
                validators: {
                    notEmpty: {
                        message: 'The Customer Order Number is required'
                    }
                }
            },
            CustomerQty: {
                validators: {
                    notEmpty: {
                        message: 'The Customer Quantity is required'
                    },
                    numeric: {
                        message: 'The Customer Quantity must be a numeric'
                    }
                }
            },
            ModelCode: {
                validators: {
                    notEmpty: {
                        message: 'The Model is required'
                    }
                }
            },
            //User
            UserName: {
                validators: {
                    notEmpty: {
                        message: 'The username is required'
                    },
                    stringLength: {
                        min: 6,
                        max: 30,
                        message: 'The username must be more than 6 and less than 30 characters long'
                    },
                }
            },
            Password: {
                validators: {
                    notEmpty: {
                        message: 'The password is required and cannot be empty'
                    },
                    stringLength: {
                        min: 8,
                        max: 15,
                        message: 'The password must be more than 8 and less than 15 characters long'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9_]+$/,
                        message: 'The password can only consist of alphabetical, number and underscore'
                    },
                }
            },
            Email: {
                validators: {
                    notEmpty: {
                        message: 'The Email Address is required'
                    },
                    emailAddress: {
                        message: 'The Email Address is not valid'
                    }
                }
            },
            Role: {
                validators: {
                    notEmpty: {
                        message: 'The Role is required'
                    },
                    numeric: {
                        message: 'The Role must be a numeric'
                    }
                }
            },
            Active: {
                validators: {
                    notEmpty: {
                        message: 'The Active status is required'
                    }
                }
            },
            //Vendor
            VendorCode: {
                validators: {
                    notEmpty: {
                        message: 'The Vendor Code is required'
                    }
                }
            },
            VendorName: {
                validators: {
                    notEmpty: {
                        message: 'The Vendor Name is required'
                    }
                }
            },
            Vendor_Type: {
                validators: {
                    notEmpty: {
                        message: 'The Vendor Type is required'
                    }
                }
            },
            Supervisor: {
                validators: {
                    notEmpty: {
                        message: 'The Supervisor is required'
                    }
                }
            },
            DeptHead: {
                validators: {
                    notEmpty: {
                        message: 'The Department tHead is required'
                    }
                }
            },
            //UserDataAccess
            User_Id: {
                validators: {
                    notEmpty: {
                        message: 'The User is required'
                    }
                }
            },
            Vendor_Id: {
                validators: {
                    notEmpty: {
                        message: 'The Vendor is required'
                    }
                }
            },
            // RoleMenuPermission
            Role_Menu: {
                validators: {
                    notEmpty: {
                        message: 'The Role Menu is required'
                    }
                }
            },
            Permission: {
                validators: {
                    notEmpty: {
                        message: 'The Permission is required'
                    }
                }
            },
            //RoleMenu
            Role_Id: {
                validators: {
                    notEmpty: {
                        message: 'The Role is required'
                    }
                }
            },
            Menu_Id: {
                validators: {
                    notEmpty: {
                        message: 'The Menu Id is required'
                    }
                }
            },
            Module: {
                validators: {
                    notEmpty: {
                        message: 'The Module is required'
                    }
                }
            }
        }
    }).on('success.form.bv', function (e) {
        e.preventDefault();
        var $form = $(div);
        var jsonData = $form.serializeObjectIntoJson();
        var jsonString = JSON.stringify(jsonData);
        var _label = "";
        var _className = "";
        $.ajax({
            url: url,
            type: "POST",
            data: { tbl: tbl, frmData: jsonString },
            success: function (data) {
                if (data.status) {
                    _label = "<i class='fa fa-check'></i> Success";
                    _className: "btn btn-success";
                } else {
                    _label = "<i class='fa fa-times'></i> Failed";
                    _className: "btn btn-danger";
                }
                bootbox.dialog({
                    message: data.message,
                    title: data.title,
                    buttons: {
                        main: {
                            label: _label,
                            className: _className,
                            callback: function () {
                            }
                        }
                    }
                });
                if (data.status) {
                    window.location.reload();
                } else {
                    validator.disableSubmitButtons(false);
                }
            },
            error: function () {
                alert('Error');
            }
        });
    });
}

function getDropDown(div, tbl, url, defvalue) {
    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'json',
        data: { tbl: tbl },
        success: function (results) {
            $.each(results, function (i, result) {
                if (result.Value == defvalue) {
                    $(div).append('<option value="' + result.Value + '" selected="selected">' + result.Text + '</option>');
                } else {

                    $(div).append('<option value="' + result.Value + '">' + result.Text + '</option>');
                }
            });
        },
        error: function (ex) {
            alert('Failed to get data.<br>Reason: ' + ex);
        }
    });
}
